import {GetStaticPaths, GetStaticProps} from 'next'

import {sampleProducts} from '../../utils/sample-data'
import {Product} from '../../interfaces/Product';
import Image from 'next/image';
import {useState} from 'react';
import StoreList from '../../components/StoreList';
import {Button} from '../../components/styled/Button';
import {Column} from '../../components/styled/Column';
import {Title} from '../../components/styled/Title';
import {Subtitle} from '../../components/styled/Subtitle';
import {useToasts} from 'react-toast-notifications';
import {sendEmail} from '../../utils/email';
import {Store} from '../../interfaces/Store';

type Props = {
    item?: Product
    errors?: string
}

const ProductDetails = ({item, errors}: Props) => {

    const [stores, setStores] = useState([]);
    const [isLoading, setLoading] = useState(false)
    const {addToast} = useToasts();

    const fetchStores = () => {
        setLoading(true)
        setStores([]);
        return fetch(`https://smeurei.teilor.ro/service/stoc_magazin?cod_produs=${item.code}`)
            .then((res) => res.json())
            .then((data) => {
                setStores(data.rows)
                setLoading(false)
                return data.rows
            })
            .catch((err) => {
                addToast('Oops.. something went wrong', {
                    appearance: 'error',
                    autoDismiss: true,
                    autoDismissTimeout: 3000
                });

                setLoading(false)
            })
    }


    const handleProductAvailabilityClick = async () => {

        fetchStores()
            .then((data: Store[]) => {
                let htmlMessage = `<p>${item?.title} having code ${item.code} is available in <br/> <br/>${data.map(store => `${store.oras}: ${store.magazin}`).join('<br/>')}</p>`;
                //TODO: do not hardcode email
                sendEmail(`george.petraru@teilor.ro`, `Availability for ${item?.title}`, htmlMessage);
            })
    };


    if (errors) {
        return (
            <p>
                <span style={{color: 'red'}}>Error:</span>
                {errors}
            </p>
        )
    }

    return (
        <div className={"flex-container"}>
            <Column flex={"45%"}>
                <Image src={item.image} alt={item.title} width={500} height={600}/>
            </Column>
            <Column flex={"55%"} marginLeft={"5vw"}>
                <Title>{item.title}</Title>
                <Title>{item.price} RON</Title>
                <Subtitle>Product Description</Subtitle>
                <p>{item.description}</p>

                {stores.length === 0 && (
                    <Button onClick={handleProductAvailabilityClick}>See product Availability</Button>
                )}
                {isLoading && (<p>Loading...</p>)}

                {stores.length !== 0 &&
                <div>
                    <p>Product availability: </p>
                    <StoreList stores={stores}/>
                </div>}
            </Column>
        </div>
    )
}

export default ProductDetails

/**
 * If a page has Dynamic Routes and uses getStaticProps, it needs to define a list of paths to be statically generated.
 * Next.js will statically pre-render all the paths specified by getStaticPaths.
 */
export const getStaticPaths: GetStaticPaths = async () => {
    // Get the paths we want to pre-render based on products
    const paths = sampleProducts.map((product) => ({
        params: {id: product.code.toString()},
    }))

    // We'll pre-render only these paths at build time.
    // { fallback: false } means other routes should 404.
    return {paths, fallback: false}
}

/**
 * Next.js will pre-render this page at build time using the props returned by the getStaticProps() function.
 * Since we are now reading from a static file, we can use the static generation rendering, but if the data would change on each request we need to move to server side rendering or client side rendering
 * This function gets called at build time on server-side
 */
export const getStaticProps: GetStaticProps = async ({params}) => {
    try {
        const id = params?.id
        const item = sampleProducts.find((product) => product.code === Number(id))
        // By returning { props: item }, the ProductDetails component will receive `item` as a prop at build time
        return {props: {item}}
    } catch (err: any) {
        return {props: {errors: err.message}}
    }
}
