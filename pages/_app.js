import '../styles/global.css'
import {Layout} from "../components/styled/Layout";
import {ToastProvider} from 'react-toast-notifications'

/**
 * This replaces the default App component.
 */
export default function App({Component, pageProps}) {
    return (
        <ToastProvider>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </ToastProvider>
    )
}