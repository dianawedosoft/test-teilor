import Link from 'next/link'
import React from 'react';
import {GetStaticProps} from 'next';
import {Product} from '../interfaces/Product';
import {sampleProducts} from '../utils/sample-data';
import {Title} from '../components/styled/Title';

type Props = {
    items: Product[]
}

const IndexPage = ({items}: Props) => (
    <div>
        <Title>Products</Title>
        <ul>
            {items.map((item) => (
                <li key={item.code}>
                    <Link href={{pathname: "/products/[id]",}} as={`/products/${item.code}`}>
                        <a>
                            {item.title}
                        </a>
                    </Link>
                </li>
            ))}
        </ul>
    </div>
)


/**
 * Next.js will pre-render this page at build time using the props returned by the getStaticProps() function.
 * Since we are now reading from a static file, we can use the static generation rendering, but if the data would change on each request we need to move to server side rendering or client side rendering
 * This function gets called at build time on server-side
 */
export const getStaticProps: GetStaticProps = async () => {
    const items: Product[] = sampleProducts
    return {props: {items}}
}

export default IndexPage
