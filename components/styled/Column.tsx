import styled from 'styled-components';

export const Column = styled.div`
    margin-left: ${props => props.marginLeft ? props.marginLeft : "0"};
    margin-right: ${props => props.marginRight ? props.marginRight : "0"};
    flex: ${props => props.flex ? props.flex : "50%"};
    align-items: center;
    align-content: center;
    position: relative;
`