import styled from 'styled-components';

export const Subtitle = styled.div`
  font-size: 1.5em;
  margin : 1em 0;
`;