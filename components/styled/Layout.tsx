import styled from 'styled-components';

export const Layout = styled.div`
  margin: 0 auto;
  padding: 10vh 10vw;;
  min-height: 100vh;
`;