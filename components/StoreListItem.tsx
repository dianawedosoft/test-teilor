import React from 'react'
import {Store} from '../interfaces/Store';


type Props = {
    store: Store
}

const StoreListItem = ({store}: Props) => (
    <div>{store.oras}: {store.magazin}</div>
)

export default StoreListItem
