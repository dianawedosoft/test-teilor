import * as React from 'react'
import StoreListItem from './StoreListItem'
import {List} from './styled/List';
import {Store} from '../interfaces/Store';

type Props = {
    stores: Store[];
}

const StoreList = ({stores}: Props) => {

    if (!stores) return <p>No stores available</p>

    return (
        <List>
            {stores.map((store) => (
                <li key={store.id_magazin}>
                    <StoreListItem store={store}/>
                </li>
            ))}
        </List>)
}

export default StoreList
