export type Product = {
    code: number
    title: string
    description: string
    price: number
    image?: string
}