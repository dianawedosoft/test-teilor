
// TODO: the variable naming should be consistent, use either camel case or snake case everywhere
export type Store = {
    id_magazin: string
    url_site: string
    magazin: string
    adresa: string
    oras: string
    regiune: string
    CodProdus: string
    Stoc: number
    stocrezComFurnizori: number
}