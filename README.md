# TypeScript Next.js

This is a simple project that gives the possibility to check a product's availability

## How to use it?

```bash
yarn install
yarn dev
```

## Build

```bash
yarn build
```
