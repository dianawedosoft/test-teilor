
export const sendEmail = async (to, subject, message) => {
    const res = await fetch("/api/sendgrid", {
        body: JSON.stringify({
            to: to,
            subject: subject,
            message: message
        }),
        headers: {
            "Content-Type": "application/json",
        },
        method: "POST",
    });

    const {error} = await res.json();
    if (error) {
        //TODO: handle error properly
        return;
    }
}
